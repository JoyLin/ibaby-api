
const {firebase, admin} = require('./firebaseConfig')


exports.GetTokenByEmail = (req, res) => {
    firebase.auth().signInWithEmailAndPassword(req.body.email, req.body.password)
    .then(function () {
         firebase.auth().currentUser.getIdToken(true)
            .then(function(idToken){
                res.send(idToken)
                res.end()
            }).catch(function (error) {
                res.send(401, error)
                res.end()
            });
    }).catch(function (error) {
        res.send(401, error)
        res.end()
    });
};


exports.VerifyToken = (req, res, next) => {
    const token = req.header('Authorization').replace('Bearer', '').trim()
    admin.auth().verifyIdToken(token)
        .then(function (decodedToken) {
            return next()
        }).catch(function (error) {
            res.status(401).send({'msg':'There is no current user.'})
            res.end()
        });
    
};

