require("firebase/auth");
const firebase = require('firebase')
const admin = require('firebase-admin')
const serviceAccount = require('./ibaby-fb85c-firebase-adminsdk-to4ob-07d29687e1.json');

var firebaseConfig = {
    apiKey: 'AIzaSyBThq-h_U1kaldjqblqYZc5ynJ3kNGKdy8',
    authDomain: 'ibaby-fb85c.firebaseapp.com',
    databaseURL: 'https://ibaby-fb85c.firebaseio.com',
    projectId: 'ibaby-fb85c',
    storageBucket: 'ibaby-fb85c.appspot.com',
    messagingSenderId: '661255001645',
    appId: '1:661255001645:ios:322d0d4657029c7cc08aa4'
};

firebase.initializeApp(firebaseConfig);
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://ibaby-fb85c.firebaseio.com"
});

module.exports = { firebase, admin };