const express = require('express')
const app = express()
const port = process.env.PORT || 8080
const cors = require('cors');
const { GetTokenByEmail, VerifyToken } = require('./authService')

app.use(cors());
app.use(express.json());

app.get('/', (req, res) => res.send('Welcome to iBaby!'))
app.post('/GetTokenByEmail', GetTokenByEmail);



app.get('/GetData', VerifyToken, (req, res) => {
    res.send({'msg':'verify success'});
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))